package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun bugfix () {
        System.out.println("SOLVE IN MASTER")
    }
    fun bugfix3 () {
        System.out.println("to-do3")
    }

    fun bugfix4 () {
        System.out.println("to-do4")
    }
    fun bugfix5 () {
        System.out.println("to-do5")
    }

}